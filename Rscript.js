const remote = require('electron').remote
const {exec} = require('child_process')
var R = require('r-script')

let win = remote.getCurrentWindow()

$("#run").click(() => {
	exec('Rscript.exe ui.R', (err, stdout, stderr) => {
		if (err) {
		console.error('exec error: $(err)');
		return;
	}

	console.log($(stdout));
	})
	win.loadURL("http://127.0.0.1:8080")
})
